﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banksoft
{
    class Client
    {
        #region Поля класса
        private string _fstName;
        private string _secName;
        private DateTime _bDay;
        private int _money;
        private int _account;
        #endregion
        #region Конструктор
        public Client()
        {
            _fstName = _secName = "NoName";
            _bDay = new DateTime(2000, 1, 1);
            _money = 0;
            _account = 0;
        }

        public Client(string fstName,string secName,DateTime bDay,int money, int account)
        {
            _fstName = fstName;
            _secName = secName;
            _bDay = bDay;
            _money = money;
            _account = account;
        }
        #endregion
        #region Методы
        public string GetAllInfo()
        {
            return "Номер счета: " + _account + " Баланс: " + _money+" руб."+"\n"+" Имя: "+_fstName+" Фамилия: "+_secName+" Дата рождения: "+_bDay.ToShortDateString();
        }

        public string GetTransactionInfo()
        {
            return "Номер счета: " + _account + " Имя: " + _fstName + " Фамилия: " + _secName;
        }

        public int GetAccount()
        {
            return _account;
        }

        public string GetfstName()
        {
            return _fstName;
        }

        public string GetsecName()
        {
            return _secName;
        }


        public bool PushMoney(int money)
        {
            if (money > 0)
            {
                _money += money;
                return true;
            }
            return false;
            }

        public bool PullMoney(int money)
        {
            if (money<=_money && money>0)
            {
                _money -= money;
                return true;
            }
            return false;
        }

        public bool TransMoney(Client client, int money)
        {
            if (money <= _money && money > 0)
            {
                _money -= money;
                client._money += money;
                return true;
            }
            return false;
        }






        #endregion


    }
}
