﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banksoft
{
    class Transactions
    {

        private Transaction[] _transactions;

        public Transactions()
        {
            _transactions = null;
        }

        public void AddNew(Transaction transaction)
        {
            if (_transactions==null)
            {
                _transactions = new Transaction[1];
                _transactions[0] = transaction;
            }
            else
            {
                Array.Resize(ref _transactions, _transactions.Length + 1);
                _transactions[_transactions.Length - 1] = transaction;
            }
        }

        public string GetAllTransactionsInfo()
        {
            string info = "";
            for (int i = 0; i < _transactions.Length; i++)
            {
                info += "Транкзация №" + (i + 1) + "\n";
                info += _transactions[i].GetAllInfo() +"\n";
                info += "******************\n";
            }
            return info;
        }


    }
}
