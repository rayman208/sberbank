﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banksoft
{
    class Program
    {
        static void Main(string[] args)
        {

            Bank bank = new Bank();
            bank.AddNewClient(new Client("Зинаида", "Petrova", new DateTime(1999, 2, 12), 20000, 12345));
            bank.AddNewClient(new Client("Pop", "Kotikov", new DateTime(1992, 4, 10), 10000, 12346));
            bank.ShowAllClients();
            Console.WriteLine("***************");
            bank.MakeTransaction(12345, 12346, 5500);

            bank.ShowAllClients();
            bank.ShowAllTransactions();

            Console.ReadKey();
        }
    }
}
