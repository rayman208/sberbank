﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banksoft
{
    class Transaction
    {
        #region Field
        private DateTime _dateTime;
        private Client _clientFrom;
        private Client _clientTo;
        private int _money;
        #endregion
        #region Procedures
        public Transaction()
        {
            _dateTime=new DateTime(2000,1,1);
            _clientFrom = new Client();
            _clientTo = new Client();
            _money = 0;
        }

        public Transaction(DateTime dateTime, Client clientFrom, Client clientTo, int money)
        {
            _dateTime = dateTime;
            _clientFrom = clientFrom;
            _clientTo = clientTo;
            _money = money;
        }
        #endregion
        #region Metods
        public string GetAllInfo()
        {
            return "Дата транзакции: " + _dateTime + "Перевод от: " + _clientFrom.GetTransactionInfo() + " Кому перевод: " + _clientTo.GetTransactionInfo() + " Сумма: " + _money;
        }
        #endregion


    }
}
