﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banksoft
{
    class Bank
    {

        private Clients _clients;
        private Transactions _transactions;

        public Bank()
        {
            _clients = new Clients();
            _transactions = new Transactions();
        }

        public void MakeTransaction(int accountFrom, int accountTo, int money)
        {
            Client clientFrom = _clients.FindClientByAccount(accountFrom);
            Client clientTo = _clients.FindClientByAccount(accountTo);
            if (clientFrom!=null && clientTo!=null)
            {
                clientFrom.TransMoney(clientTo, money);
                _transactions.AddNew(new Transaction(DateTime.Now, clientFrom, clientTo, money));
            }
        }

        public void AddNewClient(Client client)
        {
            _clients.AddNew(client);
        }

        public void ShowAllClients()
        {
            Console.WriteLine(_clients.GetAllClientsInfo());
        }

        public void ShowAllTransactions()
        {
            Console.WriteLine(_transactions.GetAllTransactionsInfo());
        }



    }
}
