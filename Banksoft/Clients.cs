﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banksoft
{
    class Clients
    {

        private Client[] _clients;
        
        public Clients()
        {
            _clients = null;
        } 

        public void AddNew(Client client)
        {
            if (_clients == null)
            {
                _clients = new Client[1];
                _clients[0] = client;
            }
            else
            {
                Array.Resize(ref _clients, _clients.Length + 1);
                _clients[_clients.Length - 1] = client;
            }
        }

        public Client FindClientByAccount(int account)
        {
            for (int i = 0; i < _clients.Length; i++)
            {
                if (_clients[i].GetAccount()==account)
                {
                    return _clients[i];
                }
            }
            return null;
        }

        public Client FindClientByfstNameAndsecName(string fstName, string secName)
        {
            for (int i = 0; i < _clients.Length; i++)
            {
                if (_clients[i].GetfstName() == fstName && _clients[i].GetsecName() == secName)
                {
                    return _clients[i];
                }
            }
            return null;
        }

        public string GetAllClientsInfo()
        {
            string info = "";
            for (int i = 0; i < _clients.Length; i++)
            {
                info += "Клиент №" + (i + 1) + "\n";
                info += _clients[i].GetAllInfo() + "\n";
                info += "******************\n";
            }
            return info;
        }
    }
}
